# oldNanaimoMines

Nanaimo was worldwide known for its fine coal and its [plentiful mines around the mid 1800's](https://www.nanaimo.ca/your-government/projects/projects-detail/coal-mine-information). Here, I share entrance locations to some of these long time ago decommissioned mines. Note that there seems to be two types of entrances: *shafts* or *slopes*.

In the following [link](http://nanaimo-coal-mines.herokuapp.com/), some of these old entrances are displayed.

To interact with this notebook on binder:

Voila [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fbearandean%2Foldmines/HEAD?urlpath=voila%2Frender%2FmineEntries.ipynb)

Notebook [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fbearandean%2Foldmines/HEAD?filepath=mineEntries.ipynb)

## Motivation

I am amazed at Nanaimo's downtown historic baggage!

...But also, this repository is a small excersice in GeoJSON usage and formatting that I came up with. Locations are loaded from the geoJSON file (*mineLocations.geojson*) contained in this repo.

## Installation

Only if you want to run the notebook, othewise I recommend visiting the [link](http://nanaimo-coal-mines.herokuapp.com/).

### Pre-requisites

Python 3+.

### How to run it

Use available requirements file to install needed modules.
In a terminal or command line type:

## Usage

```bash
jupyter notebook mineEntries.ipynb
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Credits

**Annetta Markussen-Brown**. Thank you for the first maps and documents, *you're a real rock tsar...  sorry, I meant rock star*!

[City Of Nanaimo](https://www.nanaimo.ca/)

[Vancouver Island Regional Library (VIRL)](https://virl.bc.ca/branches/nanaimo-harbourfront/)

## License

[MIT](https://choosealicense.com/licenses/mit/)
